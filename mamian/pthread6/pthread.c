#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>

#define CUS_COUNT 2
#define PRO_COUNT 2

struct msg{
    struct msg* next;
    int num;
};

struct msg* head=NULL;

pthread_cond_t cond;
pthread_mutex_t mutex;
pthread_t threads[PRO_COUNT+CUS_COUNT];

void* consumer(void* p){
    int num=*(int*)p;
    free(p);
    struct msg* mp;
    for(;;){
        pthread_mutex_lock(&mutex);
        while(head==NULL){
            printf("constomer %d is waiting...\n",num);
            pthread_cond_wait(&cond,&mutex);
        }
        mp=head;
        head=mp->next;
        pthread_mutex_unlock(&mutex);
        printf("consumer get condition:%d\n",mp->num);
        free(mp);
        sleep(2);
    }
}
void* producer(void* p){
    struct msg* mp;
    int num=*(int*)p;
    free(p);
    for(;;){
        printf("prodecer%d begin produce..\n",num);
        mp=(struct msg*)malloc(sizeof(struct msg));
        mp->num=rand()%100+1;
        printf("produce :%d\n",mp->num);
        pthread_mutex_lock(&mutex);
        mp->next=head;
        head=mp;
        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex);
        sleep(2);
    }
}

int main(){
    pthread_cond_init(&cond,NULL);
    pthread_mutex_init(&mutex,NULL);

    int i;
    for(i=0;i<CUS_COUNT;i++){
        int* p=(int*)malloc(sizeof(int));
        *p=i;
        pthread_create(&threads[i],NULL,consumer,(void*)p);
    }

    for(i=0;i<PRO_COUNT;i++){
        int* p=(int*)malloc(sizeof(int));
        *p=i;
        pthread_create(&threads[i+CUS_COUNT],NULL,producer,(void*)p);
    }

    for(i=0;i<PRO_COUNT+CUS_COUNT;i++){
        pthread_join(threads[i],NULL);
    }

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond);
}
