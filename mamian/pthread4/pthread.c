#include<stdio.h>
#include<pthread.h>
pthread_mutex_t mutex;
int ticket=100;
void* route(void* arg){
   char* pthread=(char*)arg;
   while(1){
       pthread_mutex_lock(&mutex);
       if(ticket>0){
           ticket--;
           pthread_mutex_unlock(&mutex);
           usleep(500000);
           printf("%s sells tickets:%d\n",arg,ticket);
       }else{
           pthread_mutex_unlock(&mutex);
           break;
       }
   }
}
int main(){
    pthread_t id1,id2,id3,id4;
    pthread_mutex_init(&mutex,NULL);
    pthread_create(&id1,NULL,route,"pthread1");
    pthread_create(&id2,NULL,route,"pthread2");
    pthread_create(&id3,NULL,route,"pthread3");
    pthread_create(&id4,NULL,route,"pthread4");

    pthread_join(id1,NULL);
    pthread_join(id2,NULL);
    pthread_join(id3,NULL);
    pthread_join(id4,NULL);
    pthread_mutex_destroy(&mutex);
}
