#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
int main()
{
	while (1){
		printf("myshell@localhost mydir]#");
		fflush(stdout);
		char buf[1024];
		ssize_t s = read(0, buf, sizeof(buf)-1);
		if (s > 0){
			buf[s - 1] = 0;
			printf("%s\n", buf);
		}
		char *_argv[32];
		char *start = buf;
		_argv[0] = start;
		int i = 1;
		while (*start){
			if (*start == ' '){
				*start = 0;
				start++;
				_argv[i++] = start;
            }
			else{
				start++;
			}
		}
		_argv[i] = NULL;
		pid_t id = fork();
		if (id == 0){
			execvp(_argv[0], _argv);
			exit(1);
		}
		else{
			pid_t ret=waitpid(id,NULL,0);
			if(ret>0){	
			  printf("wait success!\n");
			}
		}
	}
}
