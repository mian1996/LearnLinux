#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
void* route(void *arg){
    int *i;
    i=(int*)malloc(sizeof(int));
    *i=123;
    printf("thread 1\n");
    return i;
}
int main(){
    pthread_t tid;
    int ret;
    void* wait;
    if((ret=pthread_create(&tid,NULL,route,NULL))!=0){
        perror("pthread_create");
        exit(EXIT_FAILURE);
    }
    pthread_join(tid,&wait);
    printf("pthread:%X,ret:%d",tid,*(int*)wait);
    printf("main pthread\n");
}
