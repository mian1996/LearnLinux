#include<stdio.h>
typedef char seqqueueType; 
#define maxsize 20
typedef struct seqqueue{
    seqqueueType data[maxsize];
    size_t head;
    size_t tail;
    size_t size;
}seqqueue;
void seqqueueInit(seqqueue* q);
void seqqueuePush(seqqueue* q,seqqueueType value);
void seqqueuePop(seqqueue* q);
void seqqueueTop(seqqueue* q,seqqueueType* value);

