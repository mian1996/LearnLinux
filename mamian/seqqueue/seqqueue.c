#include"seqqueue.h"

void seqqueueInit(seqqueue* q){
    if(q==NULL){
        return;
    }
    q->size=0;
    q->head=0;
    q->tail=0;
}
void seqqueuePush(seqqueue* q,seqqueueType value){
    if(q==NULL){
        return;
    }
    if(q->size>=maxsize){
        return;
    }
    q->data[q->tail++]=value;
    q->size++;
    return;
}
void seqqueuePop(seqqueue* q){
    if(q==NULL){
        return;
    }
    --q->size;
    return;
}
void seqqueueTop(seqqueue* q,seqqueueType* value){
    if(q==NULL||value==NULL){
        return;
    }
    if(q->size==0){
        return;
    }
    *value=q->data[q->head];
    return;
}
void seqqueuePrint(seqqueue* q,seqqueueType* msg){
    if(q==NULL){
        return;
    }
    printf("\n%s\n",msg);
    size_t i=0;
    for(;i<q->size;i++){
        printf("%c ",q->data[i]);
    }
   return; 
}
void testPush(){
    seqqueue q;
    seqqueueInit(&q);
    seqqueuePush(&q,'a');
    seqqueuePush(&q,'b');
    seqqueuePush(&q,'c');
    seqqueuePush(&q,'d');
    seqqueueType tmp;
    seqqueueTop(&q,&tmp);
    printf("\nget the top value(a):%c",tmp);
    seqqueuePrint(&q,"----------------------");
    seqqueuePop(&q);
    seqqueuePop(&q);
    seqqueuePrint(&q,"----------------------");
}
int main(){
    testPush();
    return;
}
