#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>


pthread_mutex_t lock;
pthread_cond_t cond;

typedef struct _Node{
    struct _Node* next;
    int data;
}node_t,*node_p,**node_pp;


node_p alloc_node(int x){
    node_p p=(node_p)malloc(sizeof(node_t));
    if(!p){
        perror("malloc");
        exit(1);
    }

   p->data=x;
   p->next=NULL;
   return p;
}
void init_list(node_pp h){
    if(h==NULL){
        return;
    }
    *h=alloc_node(0);
}


void list_push(node_p h,int x){
    node_p p=alloc_node(x);
    p->next=h->next;
    h->next=p;
}

int is_empty(node_p head){
    return head->next==NULL?1:0;
}
void list_pop(node_p head,int* x){
    if(!is_empty(head)){
        node_p p=head->next;
        *x=p->data;
        head->next=p->next;
        free(p);
    }
}
void list_destroy(node_p h){
    int x;
    while(!is_empty(h)){
       list_pop(h,&x) ;
    }
    free(h);
}

void show_list(node_p h){
    if(!is_empty(h)){
        node_p p=h->next;
        while(p){
            printf("%d ",p->data);
            p=p->next;
        }
        printf("\n");
    }
}
void* consumer(void* arg){
    node_p head=(node_p)arg;
    while(1){
       pthread_mutex_lock(&lock);
       if(!is_empty(head)){
           int data;
           list_pop(head,&data);
           printf("consumer done! data is:%d\n",data);
       }else{
           printf("consumer waiting!\n");
           pthread_cond_wait(&cond,&lock);
       }
       pthread_mutex_unlock(&lock);
       sleep(3);
    }
}
void* product(void* arg){
    while(1){
        node_p head=(node_p)arg;
        if(is_empty(head)){
            int data;
            data=rand()%100+1;
            pthread_mutex_lock(&lock);
            list_push(head,data);
            pthread_cond_signal(&cond);
            printf("product done! data is:%d\n",data);
            pthread_mutex_unlock(&lock);
        }else{
            printf("product waiting!\n");
        }
        sleep(1);
    }
}
int main(){

   node_p head;
   init_list(&head);
   pthread_mutex_init(&lock,NULL);
   pthread_cond_init(&cond,NULL);

   pthread_t c,p;
   pthread_create(&c,NULL,consumer,(void*)head);
   pthread_create(&p,NULL,product,(void*)head);

   pthread_join(c,NULL);
   pthread_join(p,NULL);
   list_destroy(head);
   pthread_mutex_destroy(&lock);
   pthread_cond_destroy(&cond);
//   int i=0;
//   for(i=0;i<10;i++){
//       list_push(head,i);
//       show_list(head);
//   }
//  int x; 
//   for(i=0;i<10;i++){
//       list_pop(head,&x);
//       show_list(head);
//   }

}
