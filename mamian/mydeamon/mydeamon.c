#include <stdio.h>
#include<unistd.h>
#include<signal.h>
#include<stdlib.h>
void my_setsid()
{
    umask(0);
    pid_t id=fork();
    if(id>0){
        exit(0);
    }
    setsid();
    chdir("/");
    close(0);
    close(1);
    close(2);
    signal(SIGCHLD,SIG_IGN);
}
int main(){
    my_setsid();
    while(1);
    return 0;
}
