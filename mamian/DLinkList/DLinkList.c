#include "DLinkList.h"
#include<stdlib.h>
DLinkNode* CreatNode(DLinkType value){
    DLinkNode* ptr=(DLinkNode*)malloc(sizeof(DLinkNode));
    if(ptr==NULL){
        return NULL;
    }
    ptr->data=value;
    ptr->next=NULL;
    ptr->prev=NULL;
    return ptr;
}
void DLinkListInit(DLinkNode** head){
   DLinkNode* new_node=CreatNode(0);
   *head=new_node;
   (*head)->next=new_node;
   (*head)->prev=new_node;
   return;
}
  
DLinkNode* DLinkListPushBack(DLinkNode* head, DLinkType value){
    if(head==NULL){
        //非法输入
        return NULL;
    }
    int* p = NULL;
    *p = 100;
    DLinkNode* new_node=CreatNode(value);
    if(head->next==head){
        //head---new_node
        head->next=new_node;
        head->prev=new_node;
        new_node->next=head;
        new_node->prev=head;
        return;
    }
    DLinkNode* pre=head->prev;
    //head---new_node
    //pre---new_node
    pre->next=new_node;
    new_node->prev=pre;
    new_node->next=head;
    head->prev=new_node;
    return head;
} 
//////////////////////////////测试代码//////////////////////////////////////////////////////
#define PrintHeader printf("\n------------------%s-----------\n",__FUNCTION__)
void PrintChar(DLinkNode* head,DLinkType *msg)
{
    if(head==NULL) {
        //非法输入
        return;
    }
    printf("[%s]\n",msg);
    DLinkNode* cur=head->next;
    for(;cur!=head;cur=cur->next){
        printf("[%c]  ",cur->data);
    }
     printf("\n ");
     for(cur=head->prev;cur!=head;cur=cur->prev){
         printf("[%c]  ",cur->data);
     }
    return;
}
void TestInit(){
    PrintHeader;
    DLinkNode* head;
    DLinkListInit(&head);
    printf("expect NULL,actuall %p\n",head);
    printf("expect NULL,actuall %d\n",(int)head->data);
}
void TestPushBack(){
     PrintHeader;
    DLinkNode* head;
    DLinkListInit(&head);
    DLinkListPushBack(head,'a');
    DLinkListPushBack(head,'b');
    DLinkListPushBack(head,'c');
    DLinkListPushBack(head,'d');
    PrintChar(head,"尾插四个元素");
}
///////////////////////////////////main函数调用////////////////////////
int main()
{
    TestInit();
    TestPushBack();
    return 0;
}
