#include<stdio.h>
#include<pthread.h>

pthread_cond_t cond;
pthread_mutex_t mutex;

 struct msg{
    int data;
    struct msg* next;
}msg;
struct msg* head=NULL;
#define PRODUCTER 2 
#define CUSTOMER 2 

pthread_t pthread[PRODUCTER+CUSTOMER];

void* coustomer(void* arg){
   struct msg* mp;
    int num=*(int*)arg;
    for(;;){
        pthread_mutex_lock(&mutex);
        while(mp->head==NULL){
            printf("coustomer wait...\n");
            pthread_cond_wait(&cond,&wait);
        }
        mp=head;
        head=mp->next;
        pthead_mutex_unlock(&mutex);
        printf("%d coustomer wait a condition :%d\n",num,mp->data);
        free(mp);
        printf("wait producer product...\n");
        sleep(2);
    }
}
void* producer(void* arg){
    struct msg* mp;
    int num=*(int*)arg;
    for(;;){
        mp=(msg*)malloc(sizeof(msg));
        mp->data=rand()%100+1;
        printf("%d producter produce a condition :%d\n",num,mp->data);
        pthread_mutex_lock(&mutex);
        mp->next=head;
        head=mp;
        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex);
        sleep(2);
    }
}
int main(){
    pthread_cond_init(&cond,NULL);
    pthread_mutex_init(&mutex,NULL);
    for(int i=0;i<PRODUCTER;i++){
        pthread_create(&pthread[i],NULL,coustomer,(void*)i);
    }
    for(int i=0;i<PRODUCTER;i++){
        pthread_create(&pthread[PRODUCTER+i],NULL,producer,(void*)(PRODUCTER+i));
    }

    for(int i=0;i<PRODUCTER+CUSTOMER;i++){
        pthread_join(pthread[i],NULL);
    }

    pthread_cond_destroy(&cond);
    pthread_mutex_destroy(&mutex);
}
