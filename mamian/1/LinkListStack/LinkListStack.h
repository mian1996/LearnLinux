#pragma once
#include<stdio.h>
#include<stddef.h>
typedef char LinkType;
typedef struct LinkNode{
    LinkType data;
    struct LinkNode* next;
}LinkNode;
typedef struct LinkStack{
    LinkNode head;
}LinkStack;
void LinkStackInit(LinkStack* stack);
