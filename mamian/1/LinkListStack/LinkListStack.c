#include"LinkListStack.h"

void LinkStackInit(LinkStack* stack){
    if(stack==NULL){
        //非法输入
        return;
    }
    stack->head.data='\0';
    stack->head.next=NULL;
}


////////////////////////////////////////测试代码//////////////////////////////////////
#define TestHeader printf("\n===============%s=====================\n",__FUNCTION__)
void LinkStackPrint(LinkStack* stack,const char* msg){
    if(stack==NULL){
        printf("stack==NULL\n");
        return;
    }
    printf("[%s]\n",msg);
    printf("[栈顶]");
   LinkNode* cur=stack->head.next;
   for(;cur!=NULL;cur=cur->next){
       printf("[%c ],cur->data");
   }
    printf("[栈底]");
}
void TestInit(){
    TestHeader;
    LinkStack stack;
    LinkStackInit(&stack);
    LinkStackPrint(&stack,"对栈进行初始化");
}
//////////////////////////////////////////////////////////////////////////////
int main()
{
    TestInit();
    return 0; 
}
