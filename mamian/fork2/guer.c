#include<stdio.h>
#include<stdlib.h>
int main()
{
  pid_t id=fork();
  if(id==0){
	sleep(3);
	printf("子程序开始运行\n");
	printf("pid=%d,ppid=%d\n");
 }
  else if(id>0){
	sleep(1);
	printf("父程序开始运行\n");
	printf("pid=%d,ppid=%d\n");
  }
  else{
	printf("子程序创建失败\n");}
return 0;
}
