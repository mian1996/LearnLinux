#include<stdio.h>
#include<stdlib.h>
int main()
{
  pid_t id=fork();
  if(id>0){
	sleep(10);
	printf("父程序退出");
	printf("pid=%d,ppid=%d\n",getpid(),getppid());
  }
  else if(id==0){
	sleep(5);	
	printf("子程序退出");
	printf("pid=%d,ppid=%d\n",getpid(),getppid());
  }
  else{
	printf("创建子程序失败\n");
  }
return 0;
}
