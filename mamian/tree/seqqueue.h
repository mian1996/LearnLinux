#include<stdio.h>
#include<stddef.h>

#define maxsize 20
#if 1
struct TreeNode;
typedef struct  TreeNode* seqqueueType;
#else
typedef char seqqueueType; 
#endif
typedef struct seqqueue{
    seqqueueType data[maxsize];
    size_t head;
    size_t tail;
    size_t size;
}seqqueue;
void seqqueueInit(seqqueue* q);
void seqqueuePush(seqqueue* q,seqqueueType value);
void seqqueuePop(seqqueue* q);
int seqqueueTop(seqqueue* q,seqqueueType* value);

