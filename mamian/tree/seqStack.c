#include<stdio.h>
#include"seqStack.h"


void seqStackInit(seqStack* stack){
    if(stack==NULL){
        return;
    }
    stack->size=0;
}
void seqStackPush(seqStack* stack,seqStackType value){
    if(stack==NULL||value==NULL){
        return;
    }
    if(stack->size>=MAXSIZE){
        return;
    }
    stack->data[stack->size++]=value;
    return;
}
void seqStackPop(seqStack* stack){
    if(stack==NULL){
        return;
    }
    --stack->size;
}
void seqStackFront(seqStack* stack,seqStackType* value){
    if(stack==NULL){
        return 0;
    }
    *value=stack->data[stack->size-1];
    return 1;
}
void seqStackDesTroy(seqStack* stack){
    if(stack==NULL){
        return;
    }
    stack->size=0;
    return;
}
