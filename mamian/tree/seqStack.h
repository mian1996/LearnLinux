#pragma once
#define MAXSIZE 100

#if 1
struct TreeNode;
typedef struct TreeNode* seqStackType;
#else
typedef char seqStackType;
#endif
typedef struct seqStack{
   seqStackType data[MAXSIZE] ;
   size_t size;
}seqStack;
void seqStackInit(seqStack* stack);
void seqStackPush(seqStack* stack,seqStackType value)
void seqStackPop(seqStack* stack);
void seqStackFront(seqStack* stack,seqStackType* value);
void seqStackDesTroy(seqStack* stack);

