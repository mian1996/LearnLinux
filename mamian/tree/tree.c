#include"seqqueue.h"
#include"seqStack.h"
#include<string.h>
#include"tree.h"
#include<stdlib.h>
//#include<queue>
void TreeInit(TreeNode** root){
    if(root==NULL){
        return;
    }
    *root=NULL;
}

TreeNode* CreateTreeNode(TreeNodeType value){
    TreeNode* new_node=(TreeNode*)malloc(sizeof(TreeNode));
    new_node->data=value;
}
void PreOrder(TreeNode* root){
    if(root==NULL){
        printf("#");
        return;
    }
    printf("%c",root->data);
    PreOrder(root->lchild);
    PreOrder(root->rchild);
}

void InOrder(TreeNode* root){
    if(root==NULL){
        return; 
    }
    InOrder(root->lchild);
    printf("%c ",root->data);
    InOrder(root->rchild);
} 

void PostOrder(TreeNode* root){
    if(root==NULL){
        return;
    }
    PostOrder(root->lchild);
    PostOrder(root->rchild);
    printf("%c ",root->data);
    
}
//void levelorder(TreeNode* root)
//{
//    if(root == NULL){
//        return;
//    }
//    queue<TreeNode*> q;
//    q.push(root);
//    while(q.empty()){
//        TreeNode* pCur = q.top();
//        printf("%d ",pCur->data);
//        q.pop();
//        if(pCur->lchild != NULL){
//            q.push(pCur->lchild);
//        }
//        if(pCur->rchild!=NULL){
//            q.push(pCur->rchild);
//        }
//    }
//}
void LevelOrder(TreeNode* root){
    seqqueue q;
    seqqueuePush(&q,root);
    //2.循环的取队首元素
    TreeNode* cur=NULL;
    while(seqqueueTop(&q,&cur)){
    //3.访问队首元素并出队列
        printf("%c ",cur);
        seqqueuePop(&q);
        if(cur->lchild!=NULL){
            seqqueuePush(&q,cur->lchild);
        }
        if(cur->rchild!=NULL){
            seqqueuePush(&q,cur->rchild);
        }
    //4.将队首元素的左子树节点和右子树节点都依次入队列
    //5.进入下一次循环，直到队列为空，说明遍历完了
    }
}        

TreeNode* _TreeCreate(TreeNodeType array[],size_t size,size_t* index,TreeNodeType null_node){
    if(index==NULL){
        return NULL;
    }
    if(*index>=size){
        return NULL;
    }
    if(array[*index]==null_node){
        return NULL;
    }
    TreeNode* new_node=CreateTreeNode(array[*index]);
    ++(*index);
    new_node->lchild=_TreeCreate(array,size,index,null_node);
    ++(*index);
    new_node->rchild=_TreeCreate(array,size,index,null_node);
    return new_node;
}
TreeNode* TreeCreate(TreeNodeType array[],size_t size,TreeNodeType null_node){
      size_t index=0;
      return _TreeCreate(array,size,&index,null_node);
    
}
void Destroy(TreeNode* node){
    free(node);
}

TreeNode* TreeClone(TreeNode* root){
    if(root==NULL){
        return NULL;
    }
    TreeNode* new_node=CreateTreeNode(root->data);
    new_node->lchild=TreeClone(root->lchild);
    new_node->rchild=TreeClone(root->rchild);
    return new_node;
}
void TreeDestroy(TreeNode** root){
    if(root==NULL){
        return ;
    }
    if(*root==NULL){
        return;
    }
    TreeNode* to_delete=*root;
    TreeDestroy(&to_delete->lchild);
    TreeDestroy(&to_delete->rchild);
    Destroy(to_delete);
}

size_t TreeSize(TreeNode* root){
    if(root==NULL){
        return 0;
    }
    return 1+TreeSize(root->lchild)+TreeSize(root->rchild);
}

size_t TreeKLevelSize(TreeNode* root, int k){
    if(root==NULL||k<1){
        return 0;
    }
    if(k==1){
        return 1;
    }
    return TreeKLevelSize(root->lchild,k-1)+TreeKLevelSize(root->rchild,k-1);
}

size_t TreeHeight(TreeNode* root){
    if(root==NULL){
        return 0;
    }
   size_t m=TreeHeight(root->lchild) ;
   size_t n=TreeHeight(root->rchild);
   return 1+(m>n?m:n);
}

TreeNode* TreeFind(TreeNode* root, TreeNodeType to_find){
    if(root==NULL){
        return NULL;
    }
    if(root->data==to_find){
        return root;
    }
    TreeFind(root->lchild,to_find);
    TreeFind(root->rchild,to_find);
}

TreeNode* LChild(TreeNode* node){
    if(node==NULL){
        return NULL;
    }
    return node->lchild;
}
TreeNode* RChild(TreeNode* node){
    if(node==NULL){
        return NULL;
    }
    return node->rchild;
}

TreeNode* Parent(TreeNode* root, TreeNode* node){
    if(node==NULL||root==NULL){
        return NULL;
    }
    if(root->lchild==node||root->rchild==node){
        return root;
    }
    TreeNode* lret=Parent(root->lchild,node);
    TreeNode* rret=Parent(root->rchild,node);
    return lret!=NULL?lret:rret;
}


void PreOrderByLoop(TreeNode* root){
    if(root==NULL){
        return;
    }
   seqStack stack;
   seqStackInit(&stack);
   seqStackPush(&stack,root);
   TreeNode* cur=NULL;
   while(seqStackFront(&stack,&cur)){
        printf("%c ",cur);
        seqStackPop(&stack);
        if(cur->lchild!=NULL){
           seqStackPush(&stack,cur->lchild) ;
        }
        if(cur->rchild){
            seqStackPush(&stack,cur->rchild);
        }
   }
}

void InOrderByLoop(TreeNode* root){
    if(root==NULL){
        return;
    }
    seqStack stack;
    seqStackInit(&stack);
    TreeNode* cur=root;
    while(1){
        while(cur!=NULL){
            cur=cur->lchild;
            seqStackPush(&stack,&cur);
        }
        TreeNode* top=NULL;
        int ret=seqStackFront(&stack,&top);
        if(ret==0){
            return;
        }
        seqStackPop(&stack);
        cur=top->rchild;
    }
    return;
}

void PostOrderByLoop(TreeNode* root){
    if(root==NULL){
        return;
    }
    seqStack stack;
    seqStackInit(&stack);
    TreeNode* cur=root;
    TreeNode* pre=NULL;
    while(cur!=NULL){
        seqStackPush(&stack,&cur);
        cur=cur->lchild;
    }
    TreeNode* top=NULL;
    seqStackFront(&stack,&top);
    if(top->rchild==NULL||top->rchild==pre){
        printf("%c ",top);
        seqStackPop(&stack);
        pre=top;
    }else{
        cur=top->rchild;
    }
}

void swap(TreeNode** lnode,TreeNode** rnode){
    TreeNode* tmp=*lnode;
    *lnode=*rnode;
    *rnode=tmp;
    return;
}
void TreeMirror(TreeNode* root){
    if(root==NULL){
        return;
    }
    //访问动作就是交换左右子树
    swap(&root->lchild,&root->rchild);
    TreeMirror(root->lchild);
    TreeMirror(root->rchild);
}
void TreeMirrorByLoop(TreeNode* root){
    if(root==NULL){
        return;
    }
    seqqueue q;
    seqqueueInit(&q);
    seqqueuePush(&q,root);
    TreeNode* cur=NULL;
    while(seqqueueTop(&q,&cur)){
        swap(&cur->lchild,&cur->lchild);
        seqqueuePop(&q);
        if(cur->lchild!=NULL){
            seqqueuePush(&q,cur->lchild);
        }
        if(cur->rchild!=NULL){
            seqqueuePush(&q,cur->rchild);
        }
    }
    return;
}
void TreeMirrorByLoop(TreeNode* root){
    if(root==NULL){
        return;
    }
    seqqueue q;
    seqqueueInit(&q);
    seqqueuePush(&q,root);
    TreeNode* cur=NULL;
    while(seqqueueTop(&q,&cur)){
        swap(&cur->lchild,&curr->rchild);
       seqqueuePop(&q);
       if(cur->lchild!=NULL){
           seqqueuePush(&q,cur->lchild);
       }
       if(cur->rchild!=NULL){
           seqqueuePush(&q,cur->rchild);
       }
    }
}
int IsCompleteTree(TreeNode* root){
    if(root==NULL){
        return 0;
    }
    int if_start_step_two_flag=0;
    seqqueue q;
    seqqueueInit(&q);
    seqqueuePush(&q,root);
    TreeNode* cur=NULL;
    while(seqqueueFront(&q,&cur)){
        seqqueuePop(&q);
        if(if_start_step_two_flag==0){
            if(cur->lchild!=NULL&&cur->rchild!=NULL){
                seqqueuePush(&q,cur->lchild);
                seqqueuePush(&q,cur->rchild);
            }else if(cur->lchild==NULL&&cur->rchild!=NULL){
                return 0;
            }else if(cur->lchild!=NULL&&cur->rchild==NULL){
                if_start_step_two_flag=1;
                seqqueuePush(&q,cur->lchild);
            }else{
                if_start_step_two_flag=1;
            }
        }else{
            if(cur->lchild==NULL&&cur->rchild==NULL) {
                ;
            }else{
                return 0;
            }
        }
    }
}
//////////////////////////////////////////////////////////////////////////////////////
#define TestHeader  printf("\n===============================%s=================\n",__FUNCTION__)
void Testorder(){
    TestHeader;
    TreeNode* root;
    TreeInit(&root);
    TreeNode* a=CreateTreeNode('a');
    TreeNode* b=CreateTreeNode('b');
    TreeNode* c=CreateTreeNode('c');
    TreeNode* d=CreateTreeNode('d');
    TreeNode* e=CreateTreeNode('e');
    TreeNode* f=CreateTreeNode('f');
    a->lchild=b;
    a->rchild=c;
    b->lchild=d;
    b->rchild=e;
    c->rchild=f;
    root=a;
    printf("前序遍历为：");
    PreOrder(root);
    printf("\n");
    printf("中序遍历为：");
    InOrder(root);
    printf("\n");

    printf("后序遍历为：");
    PostOrder(root);
    printf("\n");
}
void testLevelPrder(){
    TestHeader;
    TreeNode* root;
    TreeInit(&root);
    TreeNode* a=CreateTreeNode('a');
    TreeNode* b=CreateTreeNode('b');
    TreeNode* c=CreateTreeNode('c');
    TreeNode* d=CreateTreeNode('d');
    TreeNode* e=CreateTreeNode('e');
    TreeNode* f=CreateTreeNode('f');
    a->lchild=b;
    a->rchild=c;
    b->lchild=d;
    b->rchild=e;
    c->rchild=f;
    root=a;
    printf("前序遍历为：");
    LevelOrder(root);
}
void TestCreateTree(){
    TestHeader;
    TreeNode* root;
    TreeNodeType array[]="abd##e##c#f##";
    root=TreeCreate(array,strlen(array),'#');
    printf("前序遍历为：");
    PreOrder(root);
    printf("\n");
    printf("中序遍历为：");
    InOrder(root);
    printf("\n");

    printf("后序遍历为：");
    PostOrder(root);
    printf("\n");

    TreeDestroy(&root);
    printf("%p",root);
}
void testclone(){
    TestHeader;
    TreeNodeType array[]="abd##e##c#f##";
    TreeNode* root=TreeCreate(array,strlen(array),'#');
    TreeNode* new_root=TreeClone(root);
    printf("\nroot:%p,new_root:%p\n",root,new_root);
}
void testTreeSzie(){
    TestHeader;
    TreeNodeType array[]="abd##e##c#f##";
    TreeNode* root=TreeCreate(array,strlen(array),'#');
    size_t size=TreeSize(root);
    printf("expect 6,actual %u\n",size);
}
void testLeveKsize(){
    TestHeader;
    TreeNodeType array[]="abd##e##c#f##";
    TreeNode* root=TreeCreate(array,strlen(array),'#');
    size_t size=TreeKLevelSize(root,3);
    printf("expect 3,actual %u\n",size);

}
void TestTreeHeight(){
    TestHeader;
    TreeNodeType array[]="abd##e##c#f##";
    TreeNode* root=TreeCreate(array,strlen(array),'#');
    size_t size=TreeHeight(root);
    printf("expect 3,actual %u\n",size);
}
void TestTreeFind(){
    TestHeader;
    TreeNodeType array[]="abd##e##c#f##";
    TreeNode* root=TreeCreate(array,strlen(array),'#');
    TreeNode* find=TreeFind(root,'c');
    printf("expect c,actual %c\n",find->data);
}
void TestLRnode(){
    TestHeader;
    TreeNodeType array[]="abd##e##c#f##";
    TreeNode* root=TreeCreate(array,strlen(array),'#');
    TreeNode* lnode=LChild(root->lchild);
    TreeNode* rnode=RChild(root->rchild);
    printf("expect lnode:d,expect:%c\n",lnode->data);
    printf("expect rnode:f,expect:%c\n",rnode->data);
}
void Testparent(){
    TestHeader;
    TreeNodeType array[]="abd##e##c#f##";
    TreeNode* root=TreeCreate(array,strlen(array),'#');
    TreeNode* parent=Parent(root,root->lchild->rchild);
    printf("expect %c,actual %c\n",root->lchild->data,parent->data);
}
void TestMirror(){
    TestHeader;
    TreeNodeType array[]="abd##e##c#f##";
    TreeNode* root=TreeCreate(array,strlen(array),'#');
    TreeMirror(root);
    InOrder(root);
    
}
void TestPreOrderByLoop(){
    TestHeader;
    TreeNodeType array[]="abd##e##c#f##";
    TreeNode* root=TreeCreate(array,strlen(array),'#');
    PreOrderByLoop(root);
}
int main(){
    Testorder();
    TestCreateTree();
    testclone();
    testTreeSzie();
    testLeveKsize();
    TestTreeHeight();
    TestTreeFind();
    TestLRnode();
    Testparent();
    TestMirror();
//    testLevelPrder();
    TestPreOrderByLoop();
}
