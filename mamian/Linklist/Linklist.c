
#include "Linklist.h"
#include<stdio.h>

#define PrintHeader printf("\n=================%s=================\n",__FUNCTION__)
void LinkListInit(LinkNode** head)
{
    if (head == NULL){
	return;
    }
    *head = NULL;
}
void Linklistprint(LinkNode* head, const char *msg)
{
    printf("[%s]\n", msg);
    LinkNode* cur = head;
    for (; cur != NULL; cur = cur->next){
	printf("[%c|%p] -> ", cur->data, cur);
    }
    printf("NULL\n");
}
LinkNode* LinkCreateNode(LinkType value)
{
    LinkNode* ptr = (LinkNode*)malloc(sizeof(LinkNode));
    ptr->data = value;
    ptr->next = NULL;
    return;
}
void LinklistFree(LinkNode* ptr)
{
    free(ptr);
}
void LinkListPushBack(LinkNode** head, LinkType value)
{
    if (head == NULL){
	return;
    }
    if (*head == NULL)
    {
	*head = LinkCreateNode(value);
	return;
    }
    LinkNode* cur = *head;
    while (cur->next != NULL){
	cur = cur->next;
    }
    cur->next = LinkCreateNode(value);

}
void LinkListPopBack(LinkNode** head)
{
    if (head == NULL){
	return;
    }
    if (*head == NULL){
	return;
    }
    LinkNode* cur = *head;
    if (cur->next == NULL){
	LinklistFree(cur);
	cur = NULL;
	return;

    }
    while (cur->next != NULL){
	if (cur->next->next == NULL){
	    LinkNode* to_delete = cur->next;
	    cur->next = NULL;
	    LinklistFree(to_delete);
	}
	else{
	    cur = cur->next;
	}
    }

}
void LinkListPushFront(LinkNode** head, LinkType value)
{
    if (head == NULL){
	return;
    }
    if (*head == NULL){
	*head = LinkCreateNode(value);
	return;
    }
    LinkNode* cur = LinkCreateNode(value);
    cur->next = *head;
    *head = cur;
    return;
}
void LinkListPopFront(LinkNode** head)
{
    if (head == NULL){
	return;
    }
    if (*head == NULL){
	return;
    }
    LinkNode* cur = *head;
    *head = (*head)->next;
    LinklistFree(cur);
    return;

}
LinkNode* LinkListFind(LinkNode* head, LinkType to_find)
{
    if (head == NULL){
	return -1;
    }
    LinkNode* cur = head;
    if (cur->data== NULL){
	return NULL;
    }
    while (cur)
    {
	if (cur->data == to_find){
	    return cur;
	}
	else{
	    cur= cur->next;
	}
    }
    return NULL;
}
void LinkListInsert(LinkNode** head, LinkNode* pos, LinkType value)//之前
{
    if (head = NULL){
	return;
    }
    if (*head == NULL){
	*head = LinkCreateNode(value);
	return;
    }
    if (*head == pos){
	LinkListPushFront(head, value);
	return;
    }
    LinkNode* cur = *head;
    while (cur->next != NULL){
	if (cur->next == pos){
	    LinkNode* NewNode = LinkCreateNode(value);
	    cur->next = NewNode;
	    NewNode->next = pos;
	    return;
	}
	else{
	    cur = cur->next;
	}
    }
    if (cur->next == pos){
	LinkListPushBack(head, value);
	return;
    }
    return;
}
//////////////////////////////////////////////测试代码////////////////////////////////////////////////////////
void TestInit()
{
    PrintHeader;
    LinkNode* head;
    LinkListInit(&head);
    printf("head=%p,%ld\n",head,head);

}
void TestPushBack()
{
    PrintHeader;
    LinkNode* head;
    LinkListInit(&head);
    LinkListPushBack(&head, 'a');
    LinkListPushBack(&head, 'b');
    LinkListPushBack(&head, 'c');
    LinkListPushBack(&head, 'd');
    Linklistprint(head, "尾插四个元素");
}
void TestLinkListPopBack()
{
    PrintHeader;
    LinkNode* head;
    LinkListInit(&head);
    LinkListPushBack(&head, 'a');
    LinkListPushBack(&head, 'b');
    LinkListPushBack(&head, 'c');
    LinkListPushBack(&head, 'd');
    Linklistprint(head, "尾插四个元素");

    LinkListPopBack(&head);
    LinkListPopBack(&head);
    Linklistprint(head, "尾删两个元素");
}
void TestLinkListPushFront()
{
    PrintHeader;
    LinkNode* head;
    LinkListInit(&head);
    LinkListPushFront(&head, 'a');
    LinkListPushFront(&head, 'b');
    LinkListPushFront(&head, 'c');
    LinkListPushFront(&head, 'd');
    Linklistprint(head, "头插四个元素");
}
void TestLinkListPopFront()
{
    PrintHeader;
    LinkNode* head;
    LinkListInit(&head);
    LinkListPopFront(&head);
    Linklistprint(head, "对空链表进行头删");

    LinkListPushFront(&head, 'a');
    LinkListPushFront(&head, 'b');
    LinkListPopFront(&head);
    Linklistprint(head, "对链表头删一次");
}

void TestLinkListFind()
{
    PrintHeader;
    LinkNode* head;
    LinkListInit(&head);
    LinkNode* ret = LinkListFind(head, 'X');
    printf("%p\n",ret);

    LinkListPushBack(&head, 'a');
    LinkListPushBack(&head, 'b');
    LinkListPushBack(&head, 'c');
    LinkListPushBack(&head, 'd');
    LinkNode* ret2=LinkListFind(head, 'a');
    printf("%p\n",ret2);

}
void TestLinkListInsert()
{
    PrintHeader;
    LinkNode* head;
    LinkListInit(&head);
    LinkListPushBack(&head, 'a');
    LinkListPushBack(&head, 'b');
    LinkListPushBack(&head, 'c');
    LinkListPushBack(&head, 'd');
    LinkListInsert(&head, 'c', 'X');
    Linklistprint(head, "在c前插入X");
}
///////////////////////////////////////////////////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^/////////////////////////////////////////////////

int main()
{
    TestInit();
    TestPushBack();
    TestLinkListPopBack();
    TestLinkListPushFront();
    TestLinkListPopFront();
    TestLinkListFind();
    TestLinkListInsert();
    system("pause");
    return;
}
