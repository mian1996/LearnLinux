#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
    while(1){
        printf("mybash->");
        fflush(stdout);
        char buf[1024];
        ssize_t s=read(0,buf,sizeof(buf)-1) ;
        if(s>0){
            buf[s-1]=0;
            printf("%s\n",buf);
        }
        char* start=buf;
        char* argv[32];
        argv[0]=start;
        int i=1;
        while(*start){
            if(*start==' '){
                *start=0;
                start++;
                argv[i++]=start;
            }else
            {
                start++;
            }
        }
        argv[i]=NULL;
        pid_t id=fork();
        if(id==0){
            execvp(argv[0],argv);
            exit(1);
        }
        if(id>0){
            pid_t wait=waitpid(id,NULL,0) ;
            if(wait>0){
                printf("wait son success!\n");
            }
        }
    }
    return 0;
}
