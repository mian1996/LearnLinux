#include<stdio.h>
#include<unistd.h>

int main(){
    int fds[2];
    if(pipe(fds)==-1){
        return 1;
    }
    pid_t id=fork();
    char *buf="hello";
    if(id==0){
        close(fds[1]);
        read(fds[0],buf,sizeof(buf)-1);
        printf("%s\n",buf);
    }else if(id>0){
        close(fds[0]);
        write(fds[1],buf,sizeof(buf));
    }
}
