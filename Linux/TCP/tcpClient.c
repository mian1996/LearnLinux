#include<stdio.h>
#include<arpa/inet.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<unistd.h>
#include<string.h>

#define MAX 128

int main(int argc,char* argv[]){
    int sock=socket(AF_INET,SOCK_STREAM,0);
    if(sock<0){
        printf("sock error!\n");
        return 1;
    }
    struct sockaddr_in server;
    server.sin_family=AF_INET;
    server.sin_port=htons(atoi(argv[2]));
    server.sin_addr.s_addr=inet_addr(argv[1]);

    if(connect(sock,(struct sockaddr*)&server,sizeof(server))<0){
        printf("connect error!\n");
        return 2;
    }
    char buf[MAX];
    while(1){
        printf("please enter# ");
        fflush(stdout);
        ssize_t s=read(0,buf,sizeof(buf)-1);
        if(s>0){
            buf[s-1]=0;
            if(strcmp("quit",buf)==0){
                printf("client quit!\n");
                break;
            }
            write(sock,buf,strlen(buf));
            s=read(sock,buf,sizeof(buf)-1);
            buf[s]=0;
            printf("Server Echo# %s\n",buf);
        }
    }
    close(sock);
    return 0;

}
