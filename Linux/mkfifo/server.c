#include<stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include<unistd.h>

int main(){
    if(mkfifo("./fifo",0644)<0) {
        perror("mkfifo");
        return 0;
    }
    int fd=open("./fifo",O_RDONLY);
    if(fd<0) {
        perror("open");
        return 1;
    }
    char buf[1024]={0};
    while(1){
        ssize_t s=read(fd,buf,sizeof(buf)-1);
        if(s>0){
            buf[s-1]=0;
            printf("client# %s",buf);
        }else if(s==0){
            printf("client quit!\n");
            break;
        }else{
            perror("write");
            break;
        }
    }
    close(fd);
    return 0;
}
