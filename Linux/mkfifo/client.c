#include<stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include<unistd.h>

int main(){
    int fd=open("./fifo",O_WRONLY);
    if(fd<0) {
        perror("open");
        return 1;
    }
    char buf[1024]={0};
    while(1){
        printf("Please Enter:");
        fgets(buf,sizeof(buf),stdin);
        write(fd,buf,sizeof(buf)-1);
    }
    close(fd);
    return 0;
}
