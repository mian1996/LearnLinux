//从键盘读取数据，写入管道，读取管道，写到屏幕
#include<stdio.h>
#include<unistd.h>
#include<string.h>

int main(){
    int fd[2]={0};
    if(pipe(fd)<0){
        perror("pipe");
        return 0;
    }
    pid_t id=fork();
    if(id<0){
        perror("fork");
        return 0;
    }
    if(id==0){
        //child write
        char* msg="你好";
        int i=0;
        for(i=0;i<5;i++){
            write(fd[1],msg,strlen(msg));
            sleep(1);
        }
        close(fd[0]);
    }else{
        //father read
        close(fd[1]);
        char buf[1024];
        while(1){
            ssize_t s=read(fd[0],buf,sizeof(buf)-1);
            if(s>0){
                buf[s]=0;
                printf("fathet get:%s\n",buf);
            }else if(s==0){
                printf("再见");
                break;
            }else{
                perror("read");
                break;
            }
        }
    }
}
