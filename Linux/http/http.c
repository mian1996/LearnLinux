#include<stdio.h>
#include<netinet/in.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<unistd.h>

int main(int argc,char* argv[]){
    if(argc!=3){
        printf("usage:%s [ip][port]\n",argv[0]);
    }
   int sock=socket(AF_INET,SOCK_STREAM,0) ;
   struct sockaddr_in addr;
    addr.sin_family=AF_INET;
    addr.sin_port=htons(atoi(argv[2]));
    addr.sin_addr.s_addr=inet_addr(argv[1]);
    
    if(bind(sock,(struct sockaddr*)&addr,sizeof(addr))<0){
        printf("bind error!\n");
        return 1;
    }
    int ret=listen(sock,10);
    if(ret<0){
        printf("listen error!\n");
        return 2;
    }
    for(;;){
        struct sockaddr_in client_sock;
        socklen_t len;//等待连接队列的最大长度
        int client_fd=accept(sock,(struct sockaddr*)&client_sock,&len);
        if(client_fd<0){
            printf("accept error!\n");
           continue; 
        }
        char input_buf[1024]={0};
        ssize_t read_size=read(client_fd,input_buf,sizeof(input_buf));
        if(read_size<0){
            return 2;
        }
        printf("[Request] %s",input_buf);
        char buf[1024]={0};
        const char* hello="<h1hello mamian</h1>";
        
        sprintf(buf,"HTTP/1.0 200 ok\nContent-Length:%lu\n]n%s",strlen(hello),hello);
        write(client_fd,buf,strlen(buf));

    }
    return 0;
}
