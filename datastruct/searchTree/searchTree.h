typedef char SearchType; 

typedef struct  SearchNode { 
    SearchType key; // 关键码 
    struct  SearchNode* lchild; 
    struct  SearchNode* rchild; 
}SearchNode; 

void SearchInit( SearchNode** root); 

void SearchInsert( SearchNode** root, SearchType key); 

SearchNode* SearchFind(SearchNode* root, 
        SearchType to_find); 

void SearchRemove( SearchNode** root, SearchType key); 
