#include"searchTree.h"
#include<stdio.h>
#include<stdlib.h>

void SearchInit(SearchNode** root){
    if(root==NULL){
        return;
    }
    *root=NULL;
    return;
}

SearchNode*  CreatSearchNode(SearchType key){
    SearchNode* new_node=(SearchNode*)malloc(sizeof(SearchNode));
    new_node->key=key;
    new_node->lchild=NULL;
    new_node->rchild=NULL;
    return new_node;
}
void SearchInsert(SearchNode** root, SearchType key){
    if(root==NULL){
        return;
    }
    if(*root==NULL){
        SearchNode* new_node=CreatSearchNode(key);
        *root=new_node;
        return;
    }
    SearchNode* cur=*root;
    if(key>cur->key){
        SearchInsert(&cur->rchild,key);
    }else if(key<cur->key){
        SearchInsert(&cur->lchild,key);
    }else{
        //我们约定，不允许出现重复的数据
        return;
    }
    return;
}

SearchNode* SearchFindByLoop(SearchNode* root,SearchType to_find){
    if(root==NULL){
        return NULL;
    }
    SearchNode* cur=root;
    if(to_find<cur->key){
        SearchFind(cur->lchild,to_find);
    }else if(to_find>cur->key){
        SearchFind(cur->rchild,to_find);
    }else{
        return cur;
    }
}

SearchNode* SearchFind(SearchNode* root,SearchType to_find){
    if(root==NULL){
        return NULL;
    }
    SearchNode* cur=root;
    while(cur!=NULL){
        if(to_find<cur->key){
            cur=cur->lchild;
        }else if(to_find>cur->key){
            cur=cur->rchild;
        }else{
            return cur;
        }
    }
}

void SearchRemove( SearchNode** root, SearchType key){
    if(root==NULL){
        return;
    }
    if(*root==NULL){
        return;
    }
    SearchNode* to_remove=*root;
    SearchNode* parent=NULL;
    while(to_remove!=NULL){
        if(key<to_remove->key){
            parent=to_remove;
            to_remove=to_remove->lchild;
        }else if(key>to_remove->key){
            parent=to_remove;
            to_remove=to_remove->rchild;
        }else{
           break; 
        }
    }
    if(to_remove==NULL){
        printf("没找到节点\n");
    }
    if(to_remove->lchild==NULL&&to_remove->rchild==NULL){
        //删除节点没有子树，直接删除
        if(to_remove==*root){
            *root==NULL;
        }
        if(parent->lchild==to_remove){
            parent->lchild=NULL;
        }else{
            parent->rchild=NULL;
        }
        free(to_remove);
        return;
    }else if(to_remove->lchild!=NULL&&to_remove->rchild==NULL){
        if(to_remove==*root){
            *root=to_remove->lchild;
        }else{
            if(parent->lchild==to_remove){
                parent->lchild=to_remove->lchild;
            }else{
                parent->rchild=to_remove->lchild;
            }
        }
        free(to_remove);
        return; 
    }else if(to_remove->lchild==NULL&&to_remove->rchild!=NULL){
        if(to_remove==*root) {
            *root=to_remove->rchild;
        }else{
            if(parent->lchild==to_remove){
                parent->lchild=to_remove->rchild;
            }else{
                parent->rchild=to_remove->rchild;
            }
        }
        free(to_remove);
        return;
    }else{
        //找到删除节点右子树的最小值，赋值给删除节点，删除最小直节点
        SearchNode* min=to_remove->rchild;
        SearchNode* min_parent=to_remove;
        while(min->lchild!=NULL){
            min_parent=min;
            min=min->lchild;
        }
        to_remove->key=min->key;
        if(min_parent->lchild==min){
            min_parent->lchild=min->rchild;
        }else{
            min_parent->rchild=min->rchild;
        }
        free(min);
        return;
    }
}
#define PrintHeader printf("\n===============%s==============\n",__FUNCTION__)
void TestInit(){
    PrintHeader;
    SearchNode* root;
    SearchInit(&root);
    printf("tree expect NULL,actual %p\n",root);
}

void searchPreOrder(SearchNode* root){
    if(root==NULL){
        return;
    }
    printf("%c ",root->key);
    searchPreOrder(root->lchild);
    searchPreOrder(root->rchild);
    return;
}

void searchInOrder(SearchNode* root){
    if(root==NULL){
        return;
    }
    searchInOrder(root->lchild);
    printf("%c ",root->key);
    searchInOrder(root->rchild);
    return;
}
void TestInsert(){
    PrintHeader;
    SearchNode* root;
    SearchInit(&root);
    SearchInsert(&root,'a');
    SearchInsert(&root,'c');
    SearchInsert(&root,'f');
    SearchInsert(&root,'b');
    SearchInsert(&root,'e');
    printf("preOreder:\n");
    searchPreOrder(root);
    printf("\n");
    printf("preOreder:\n");
    searchInOrder(root);
}
void TestFind(){
    PrintHeader;
    SearchNode* root;
    SearchInit(&root);
    SearchInsert(&root,'a');
    SearchInsert(&root,'c');
    SearchInsert(&root,'f');
    SearchInsert(&root,'b');
    SearchInsert(&root,'e');
    SearchNode* ret=SearchFindByLoop(root,'f');
    SearchNode* ret2=SearchFind(root,'b');
    printf("ecpectel f,actual %c",ret->key);
    printf("ecpectel b,actual %c",ret2->key);
}
void TestRemove(){
    PrintHeader;
    SearchNode* root;
    SearchInit(&root);
    SearchInsert(&root,'a');
    SearchInsert(&root,'c');
    SearchInsert(&root,'f');
    SearchInsert(&root,'b');
    SearchInsert(&root,'e');
    SearchRemove(&root,'b');
    searchPreOrder(root);
    printf("\n");
    searchInOrder(root);

}
int main(){
    TestInit();
    TestInsert();
    TestFind();
    TestRemove();
    return 0;
}
