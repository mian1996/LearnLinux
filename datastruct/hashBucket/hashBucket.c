#include"hashBucket.h"
#include<stdio.h>
#include<stdlib.h>
HashElem* CreateElem(keyType key,valType value){
    HashElem* new_node=(HashElem*)malloc(sizeof(HashElem));
    new_node->key=key;
    new_node->value=value;
    new_node->next=NULL;
    return new_node;
}

void HashFree(HashElem* cur){
    free(cur);
    return;
}
size_t hash_func(keyType key){
    return key%HashMaxsize;
}
void HashInit(hashBucket* ht,HashFunc func){
    if(ht==NULL){
        return;
    }
    ht->size=0;
    ht->func=func;
    size_t i=0;
    for(;i<HashMaxsize;i++){
        ht->data[i]=NULL;
    }
   return; 
}

void HashDestroy(hashBucket* ht){
    if(ht==NULL){
        return;
    }
    ht->size=0;
    ht->func=NULL;
    size_t i=0;
    for(;i<HashMaxsize;i++){
        HashElem* cur=ht->data[i];
        while(cur!=NULL){
            HashElem* next=cur->next;
            HashFree(cur);
            cur=next;
        }
    }
}

void HashInsert(hashBucket* ht,keyType key,valType value)
{
    if(ht==NULL){
        return;
    }
    size_t offset=ht->func(key);
    HashElem* new_node=CreateElem(key,value);
    int ret=HashFind(ht,key);
    if(ret==1){
        return;
    }else{
        HashElem* new_node=CreateElem(key,value);
        new_node->next=ht->data[offset];
        ht->data[offset]=new_node;
        ++ht->size;
        return;
    }
}

int HashFind(hashBucket* ht,keyType key){
    if(ht==NULL){
        return 0;
    }
    size_t offset=ht->func(key);
    if(ht->data[offset]==NULL){
        return 0;
    }
    HashElem* cur=ht->data[offset];
    while(cur!=NULL){
        if(cur->key==key){
            return 1;
        }else{
            cur=cur->next;
        }
    }
    return 0;
}

int HashRemove(hashBucket* ht,keyType key){
    if(ht==NULL){
        return 0;
    }
    if(ht->size==0){
        return 0;
    }
    size_t offset=ht->func(key);
    if(ht->data[offset]==NULL){
        return 0;
    }
    HashElem* pre=ht->data[offset];
    if(pre->key==key){
        HashFree(pre);
        pre=NULL;
        return 1;
    }
    HashElem* cur=pre->next;
    while(cur!=NULL){
        if(cur->key==key){
           pre->next=cur->next;
           HashFree(cur);
           return 1;
        }
        pre=cur;
        cur=cur->next;
    }
    return 0;
}
//////////////////////////////////
#define HeaderPrint printf("\n=========%s======\n",__FUNCTION__)
void HashPrint(hashBucket* ht,const char* msg){
    if(ht==NULL){
        return;
    }
    printf("[%s]\n",msg);
    size_t i=0;
    for(;i<HashMaxsize;i++){
        if(ht->data[i]!=NULL){
            HashElem* cur=ht->data[i];
            while(cur!=NULL){
                printf("[%u] key:%d value:%d-> ",i,cur->key,cur->value);
                cur=cur->next;
            }
            printf("\n");
        }
    }
}
void TestInsert(){
    HeaderPrint;
    hashBucket ht;
    HashInit(&ht,hash_func);
    HashInsert(&ht,1,100);
    HashInsert(&ht,11,100);
    HashInsert(&ht,111,100);
    HashInsert(&ht,15,100);
    HashInsert(&ht,5,17);
    HashInsert(&ht,80,10);
    HashInsert(&ht,3,88);
    HashPrint(&ht,"往哈希表插入四个元素");
}
void TestFind(){
    HeaderPrint;
    hashBucket ht;
    HashInit(&ht,hash_func);
    HashInsert(&ht,1,100);
    HashInsert(&ht,11,100);
    HashInsert(&ht,111,100);
    HashInsert(&ht,15,100);
    HashInsert(&ht,5,17);
    HashInsert(&ht,80,10);
    HashInsert(&ht,3,88);
    int ret=HashFind(&ht,11);
    printf("expect 1,actual %d\n",ret);
    int ret1=HashFind(&ht,6);
    printf("expect 0,actual %d\n",ret1);
}
void TestRemove(){
    HeaderPrint;
    hashBucket ht;
    HashInit(&ht,hash_func);
    HashInsert(&ht,1,100);
    HashInsert(&ht,11,100);
    HashInsert(&ht,111,100);
    HashInsert(&ht,15,100);
    HashInsert(&ht,5,17);
    HashInsert(&ht,80,10);
    HashInsert(&ht,3,88);
    HashPrint(&ht,"往哈希表插入四个元素");
    int ret=HashRemove(&ht,11);
    printf("expect 1,actual %d\n",ret);
    HashPrint(&ht,"往哈希表插入四个元素");
}
int main(){
    TestInsert();
    TestFind();
    TestRemove();
}
