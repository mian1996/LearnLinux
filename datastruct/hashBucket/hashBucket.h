#include<stddef.h>
#define HashMaxsize 10
typedef int keyType;
typedef int valType;

typedef size_t (*HashFunc)(keyType key);
typedef struct HashElem{
    keyType key;
    valType value;
    struct HashElem* next;
}HashElem;

typedef struct hashBucket{
    HashElem*  data[HashMaxsize];
    size_t size;
    HashFunc func;
}hashBucket;

void HashInit(hashBucket* ht,HashFunc func);

void HashDestroy(hashBucket* ht);

void HashInsert(hashBucket* ht,keyType key,valType value);

int HashFind(hashBucket* ht,keyType key);

int HashRemove(hashBucket* ht,keyType key);


