
#include<stddef.h>

typedef int keyType;
typedef int valueType;
typedef size_t (*hashFuncDefault)(keyType key);

typedef enum Stat{
    Empty,
    Valid,
    Deleted
}Stat;
#define HashMaxSize 1000
typedef struct hashElem{
    keyType key;
    valueType value;
    Stat stat;//引入一个stat标记作为是否有效的标记 
}hashElem;

typedef struct hashTable{
    hashElem data[HashMaxSize];
    size_t size;
    hashFuncDefault hash_func;//哈希函数
}hashTable;

void HashInit(hashTable* ht,hashFuncDefault hash_func);

int HashInsert(hashTable* ht,keyType key,valueType value);

int HashFind(hashTable* ht,keyType key,valueType* value);

void HashRemove(hashTable* ht,keyType key);

int HashEmpty(hashTable* ht);
size_t HashSize(hashTable* ht);
void HashDestroy(hashTable* ht);
