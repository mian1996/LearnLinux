#include"hash_table.h"
#include<stdio.h>
 
size_t HashFunc(keyType key){
    return key%HashMaxSize;
}
void HashInit(hashTable* ht,hashFuncDefault hash_func)
{
    if(ht==NULL){
        return;
    }
    ht->size=0;
    ht->hash_func=hash_func;
    size_t i=0;
    for(;i<HashMaxSize;i++){
        ht->data[i].stat=Empty;
    }
    return;
}

int HashInsert(hashTable* ht,keyType key,valueType value){
    if(ht==NULL){
        return 0;
    }
    if(ht->size>=HashMaxSize*0.8){
        return 0;
    }
    //2.根据key来计算offset
    size_t offset=ht->hash_func(key);
    //3.从offset位置开始线性的往后找，找到第一个状态为empty这样的元素来插入。
    while(1){
        if(ht->data[offset].stat!=Valid){
            ht->data[offset].key=key;
            ht->data[offset].value=value;
            ht->data[offset].stat=Valid;
            ++ht->size;
            return 1;
        }else{
            if(ht->data[offset].key==key){
                return 0;
            }
            ++offset;
        }
    }
    //4.如果发现了key相同的元素，此时认为插入失败。
    //5.++size
}

int HashFind(hashTable* ht,keyType key,valueType* value){
    if(ht==NULL){
        return 0;
    }
    //1.根据key计算出offset
    //2.从offset开始往后开始查找，每次取到一个元素，使用key进行比较
    //如果找到了key相同的元素，此时直接把value返回回去进行，并且认为查找成功。
    //如果发现当前的key不相同，就继续往后查找
    //如果发现当前的元素是一个空元素，此时认为查找失败
    size_t offset=ht->hash_func(key);
    while(1){
        if(ht->data[offset].key==key&&ht->data[offset].stat==Valid){
            *value=ht->data[offset].value;
            return 1;
        }else{
            if(ht->data[offset].stat==Empty){
                return 0;
            }
            ++offset;
        }
    }

}

void HashRemove(hashTable* ht,keyType key){
    if(ht==NULL){
        return;
    }
    //1.根据key计算offset
    //2.从offset开始，一次判定当前元素的key和要删除元素的key是不是相同
    //如果当前的key就是要删除的key，删除当前元素即可，删除元素要引入一个新的状态标记Deleted
    //如果当前的元素为空元素，key在hash表中没有找到，删除失败
    //剩下的情况++offset，线性探测尝试查找下一个元素。
    size_t offset=ht->hash_func(key);
    while(1){
        if(ht->data[offset].key==key&&ht->data[offset].stat==Valid){
            ht->data[offset].stat=Deleted;
            --ht->size;
            return;
        }else{
            if(ht->data[offset].stat==Empty){
                printf("key不存在\n");
                return;
            }
            ++offset;
        }
    }
}

int HashEmpty(hashTable* ht){
    if(ht==NULL){
        return 1;
    }
    return ht->size==0?1:0
}

size_t HashSize(hashTable* ht){
    if(ht==NULL){
        return 0;
    }
    return ht->size;
}
void HashDestroy(hashTable* ht){
    if(ht==NULL){
        return;
    }
    size_t i=0;
    for(;i<HashMaxSize;i++){
        ht->data[i].stat=Empty;
    }
    ht->size=0;
    ht->hash_func=NULL;
    return;
}
///////////////////////
#define HeaderPrint printf("\n==========%s=========\n",__FUNCTION__)
void HashPrint(hashTable* ht,const char* msg){
    if(ht==NULL){
        return;
    }
    printf("%s\n",msg);
    size_t i=0;
    for(;i<HashMaxSize*0.8;i++){
        if(ht->data[i].stat==Valid){
            printf("[%u]key:%lu value:%lu\n",i,ht->data[i].key,ht->data[i].value);
        }
    }
}

void TesthashInit(){
    HeaderPrint;
    hashTable ht;
    HashInit(&ht,HashFunc);
    HashInsert(&ht,1,1);
    HashInsert(&ht,2,11);
    HashInsert(&ht,5,9);
    HashInsert(&ht,101,1);
    HashInsert(&ht,101,2);
    HashPrint(&ht,"往哈希表插入四个元素");
}
void TestFind(){
    HeaderPrint;
    hashTable ht;
    HashInit(&ht,HashFunc);
    HashInsert(&ht,1,1);
    HashInsert(&ht,2,11);
    HashInsert(&ht,5,9);
    HashInsert(&ht,101,1);
    valueType value;
    int ret=HashFind(&ht,102,&value);
    printf("expect 0,actual %d\n",ret);
    int ret1=HashFind(&ht,2,&value);
    printf("expect 11,actual %d\n",value);
}
void TestRemove(){
    HeaderPrint;
    hashTable ht;
    HashInit(&ht,HashFunc);
    HashInsert(&ht,1,1);
    HashInsert(&ht,2,11);
    HashInsert(&ht,5,9);
    HashInsert(&ht,101,1);
    HashRemove(&ht,5);
    HashPrint(&ht,"删除一个哈希值5");
}
int main(){
    TesthashInit();
    TestFind();
    TestRemove();
}
