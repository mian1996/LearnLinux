#include<stdint.h>
#include<stddef.h>
typedef struct Bitmap{
    uint64_t* data;
    uint64_t capacity;
}Bitmap;

void BitMapInit(Bitmap* bm,size_t capacity);

void BitMapSet(Bitmap* bm,size_t index);

void BitMapUnset(Bitmap* bm,size_t index);

void BitMapFill(Bitmap* bm);

void BitMapClear(Bitmap* bm);

void BitMapDestroy(Bitmap* bm);

int BitMapTest(Bitmap* bm,size_t index);
