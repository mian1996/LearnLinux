#include"bitmp.h"
typedef uint64_t (*BloomHash)(const char*);
#define BloomHashCount 2

typedef struct BloomFilter{
   Bitmp bm;
   BloomHash bloomhash[BloomHashCount];
}BloomFilter;

void BloomFilterInit(BloomFilter* bf);
void BloomFilterDestroy(BloomFilter* bf);
void BloomFilterInsert(BloomFilter* bf,const char* str);
void BloomFilterIsExit(BloomFilter* bf,const char* str);
