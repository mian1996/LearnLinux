#include"bitmp.h"
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
size_t  DataSize(size_t capacity){
    return capacity/(sizeof(uint64_t)*8)+1;
}
void BitMapInit(Bitmap* bm,size_t capacity)
{
    if(bm==NULL){
        return;
    }
    bm->capacity=capacity;
    size_t size=DataSize(capacity);
    bm->data=(uint64_t*)malloc(sizeof(uint64_t)*size);
    memset(bm->data,0,sizeof(uint64_t)*size);
}

void GetOffset(uint64_t index,uint64_t* n,uint64_t* offset){
    *n=index/(sizeof(uint64_t)*8);
    *offset=index%(sizeof(uint64_t)*8);
    return;
}
void BitMapSet(Bitmap* bm,size_t index){
    if(bm==NULL){
        return;
    }
    if(index>=bm->capacity){
        return;
    }
    uint64_t n,offset;
    GetOffset(index,&n,&offset);
    bm->data[n]|=(0x1ul<<offset);
}
void BitMapUnset(Bitmap* bm,size_t index)
{
    if(bm==NULL){
        return;
    }
    if(index>=bm->capacity){
        return;
    }
    uint64_t n,offset;
    GetOffset(index,&n,&offset);
    bm->data[n]&=~(0x1ul<<offset);
}

void BitMapClear(Bitmap* bm)
{
    if(bm==NULL){
        return;
    }
    memset(bm->data,0,sizeof(uint64_t)*DataSize(bm->capacity));
    return;
}
void BitMapFill(Bitmap* bm){
    if(bm==NULL){
        return;
    }
    memset(bm->data,0xff,sizeof(uint64_t)*DataSize(bm->capacity));
    return;
}
void BitMapDestroy(Bitmap* bm){
    //TODO
}
int BitMapTest(Bitmap* bm,size_t index)
{
    if(bm==NULL){
        return 0;
    }
    if(index>=bm->capacity){
        return 0;
    }
    uint64_t n,offset;
    GetOffset(index,&n,&offset);
    uint64_t ret=bm->data[n]&(1ul<<offset);
    return ret!=0?1:0;
}
#define HeadPrint printf("\n======%s========\n",__FUNCTION__)
void TestInit(){
    HeadPrint;
    Bitmap bm;
    BitMapInit(&bm,65);
    printf("size expect 65,actual %u\n",bm.capacity);
    return;
}
void BitMapPrint(Bitmap* bm){
    size_t i=0;
    for(;i<bm->capacity/(sizeof(uint64_t)*8)+1;++i){
        printf("[%lu] %lx ",i,bm->data[i]);
    }
}
void TestSet(){
    HeadPrint;
    Bitmap bm;
    BitMapInit(&bm,65);
    BitMapSet(&bm,31);
    BitMapSet(&bm,32);
    BitMapSet(&bm,33);
    BitMapSet(&bm,1);
    BitMapPrint(&bm);
}
void TestUset(){
    HeadPrint;
    Bitmap bm;
    BitMapInit(&bm,65);
    BitMapSet(&bm,31);
    BitMapSet(&bm,32);
    BitMapSet(&bm,33);
    BitMapSet(&bm,1);
    BitMapUnset(&bm,31);
    BitMapPrint(&bm);
}
void TestFill(){
    HeadPrint;
    Bitmap bm;
    BitMapInit(&bm,65);
    BitMapFill(&bm);
    BitMapPrint(&bm);
}
int main(){
    TestInit();
    TestSet();
    TestUset();
    TestFill();
}

